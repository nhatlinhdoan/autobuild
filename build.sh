#!/usr/bin/env bash

umask 022

if [ -z "$SUBMIT" ]; then
    SUBMIT=1
fi

if [ -z "$SLACK_HOOK" ]; then
    echo "[ERROR] SLACK_HOOK variable is missing"
    exit 1
fi

if [ -z "$SUBMIT_URL" ]; then
    echo "[ERROR] SUBMIT_URL variable is missing"
    exit 1
fi

set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable

# env vars
export LC_ALL="en_US.UTF-8"
export TZ=UTC

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
LOCKFILE="/tmp/.build-dockerfile"
BUILD_TARGET="$@"
BUILD_TARGET_SUBMIT="$@"
RETURN_CODE="0"
BG_PID=""
COLUMNS=100

function lockfile() {
    exec {lock_fd}>$LOCKFILE || exit 1
    flock -n "$lock_fd" || { echo "ERROR: flock() failed." >&2; exit 1; }
}

function clearDockerEnv() {
    docker system prune -f
}

lockfile

BUILD_TARGET="build test push"
START_TIME="$(date +%s)"

git checkout master

make setup
make provision

GIT_COMMIT="$(git rev-parse HEAD)"
GIT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
DOCKER_VERSION="$(docker --version)"
KERNEL_VERSION="$(uname -r)"
LOGFILE=`mktemp build.XXXXXXXXXX`
BLACKLIST="$(cat BLACKLIST)"

echo > "$LOGFILE"

exec >  >(tee -a $LOGFILE)
exec 2> >(tee -a $LOGFILE >&2)

toLog() {
    cat
}

logMsg() {
    echo "$*"
}

#################################################
# Background process handling
#################################################

initPidList() {
    unset PID_LIST
    declare -a PID_LIST
    PID_LIST=()
}

addBackgroundPidToList() {
    if [[ -z "$BG_PID" ]]; then
        BG_PID="$!"
    fi

    if [ "$#" -eq 0 ]; then
        PID_LIST[$BG_PID]="$BG_PID"
    else
        PID_LIST[$BG_PID]="$*"
    fi

    BG_PID=""
}

waitForBackgroundProcesses() {
    local WAIT_BG_RETURN=0

    ## check if pidlist exists
    if [[ -z "${PID_LIST[@]:+${PID_LIST[@]}}" ]]; then
        logMsg " -> No background processes found"
        return
    fi

    while [ 1 ]; do

        for pid in "${!PID_LIST[@]}"; do
            title=${PID_LIST[$pid]}

            # check if pid is finished
            if ! kill -0 "$pid" 2> /dev/null; then
                # get return code
                if wait "$pid" 2> /dev/null; then
                    # success
                    logMsg ""
                    logMsg " -> Process ${title} finished successfully"
                    unset PID_LIST[$pid]
                else
                    # failed
                    logMsg " -> Process ${title} FAILED"
                    WAIT_BG_RETURN=1
                    RETURN_CODE=1
                    unset PID_LIST[$pid]
                fi

            fi
        done

        if [ "${#PID_LIST[@]}" -eq 0 ]; then

            # check if any subprocess failed
            if [ "$WAIT_BG_RETURN" -ne 0 ]; then
                logMsg " [!!!] Build FAILED at $(date)"
                RETURN_CODE="$WAIT_BG_RETURN"
            else
                logMsg " -> Build finished at $(date)"
            fi

            break
        fi

        sleep 15
        sendResult
    done

    initPidList
}

function sendResult() {
    if [ "$SUBMIT" -eq 1 ]; then
        curl \
            -F "starttime=${START_TIME}" \
            -F "commit=${GIT_COMMIT}" \
            -F "branch=${GIT_BRANCH}" \
            -F "docker_version=${DOCKER_VERSION}" \
            -F "kernel_version=${KERNEL_VERSION}" \
            -F "type=docker" \
            -F "hostname=$(hostname)" \
            -F "target=${BUILD_TARGET_SUBMIT}" \
            -F "blacklist=${BLACKLIST}" \
            -F "log=@${LOGFILE}" \
            "$SUBMIT_URL" &> /dev/null
    fi
}

function sendResultFinal() {
    if [ "$SUBMIT" -eq 1 ]; then
        curl \
            -F "starttime=${START_TIME}" \
            -F "endtime=${END_TIME}" \
            -F "commit=${GIT_COMMIT}" \
            -F "branch=${GIT_BRANCH}" \
            -F "docker_version=${DOCKER_VERSION}" \
            -F "kernel_version=${KERNEL_VERSION}" \
            -F "type=docker" \
            -F "hostname=$(hostname)" \
            -F "target=${BUILD_TARGET_SUBMIT}" \
            -F "blacklist=${BLACKLIST}" \
            -F "return_code=${RETURN_CODE}" \
            -F "log=@${LOGFILE}" \
            "$SUBMIT_URL" &> /dev/null
    fi


    if [[ "${RETURN_CODE}" -eq 0 ]]; then
        curl -X POST --data-urlencode 'payload={"channel": "#development", "username": "slave.webdevops.io", "text": ":white_check_mark: Docker images built successfully", "icon_emoji": ":package:"}' $SLACK_HOOK
    else
        curl -X POST --data-urlencode 'payload={"channel": "#development", "username": "slave.webdevops.io", "text": ":boom: Docker image build FAILED", "icon_emoji": ":package:"}' $SLACK_HOOK
    fi
}

function printLine() {
    printf "${1}%.0s" $(seq 1 ${COLUMNS})
    echo
}

initPidList

## Run task
sendResult

for makeParam in $BUILD_TARGET; do
    initPidList

    printLine "=" | toLog
    logMsg "==> make ${makeParam}"
    printLine "=" | toLog
    logMsg ""
    logMsg ""

    (
	set -o pipefail  ## trace ERR through pipes
	set -o errtrace  ## trace ERR through 'time command' and other functions
	set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
	set -o errexit   ## set -e : exit the script if any statement returns a non-true return value
	make "$makeParam" | ts '[%b %d %H:%M:%S]'
    ) &
    addBackgroundPidToList
    waitForBackgroundProcesses

    sleep 5

    if [[ "$RETURN_CODE" -ne 0 ]]; then
        break
    fi
done

END_TIME="$(date +%s)"


echo ""

sendResultFinal

## Submit
echo ""
echo ""
echo "Submitted result"
echo ""
echo ""
echo ""

rm -f "$LOGFILE"

clearDockerEnv

exit "$RETURN_CODE"

